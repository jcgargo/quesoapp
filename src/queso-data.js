import { LitElement, html, css } from 'lit-element';  

class QuesoData extends LitElement {

    static get properties() {
        return {
            queso: {type: Object}
        };
    }
    
    static get styles() {

        return css`
            .borde {
                padding: 30px 0px 30px 0px;
            }
        `
    }

	constructor() {
		// Always calls super() first.
		super();
        this.queso = {};
	}
		
	render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">        
            
            <div class="col">
                <div class="card text-center border-primary mb-3 h-100">
                    <img class="card-img-top" src="${this.queso.image.src}" alt="Card image cap">
                    <div class="car-body">
                        <div class="card-header text-primary">${this.queso.name} </div> 
                        <div class="text-center align-middle text-primary" style="height: 50px;"><h1>Edad: ${this.queso.age}</h1></div>
                        <div class="">
                            <img src="./img/${this.queso.recommended}.png"></img>
                        </div>
                    </div>
                    <div class="card-footer text-end">
                        <a name="elimina" class="btn btn-danger" @click="${this.elimina}">Eliminar</a>
                        <a name="elimina" class="btn btn-primary" @click="${this.info}">Info</a>
                    </div>
                </div>
            </div>
		`;
	}

    elimina(e) {
        console.log("Elimina " + this.queso.name);
        this.dispatchEvent(new CustomEvent("elimina-queso", {detail: {
            "name": this.queso.name
        }}));        
    }

    info(e) {
        console.log(this.queso);
        this.dispatchEvent(new CustomEvent("info-queso", {detail: {
            "queso": this.queso
        }}));        
    }
}
customElements.define('queso-data', QuesoData)