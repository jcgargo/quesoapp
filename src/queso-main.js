import { LitElement, html, css } from 'lit-element';  
import './queso-data.js'
import './queso-form.js'

export class QuesoMain extends LitElement {

    static get properties() {
        return {
            lista: {type: Array},
            accion: {type: String}    
        };
    }
    
    static get styles() {

        return css`
        `
    }

	constructor() {
		// Always calls super() first.
		super();
        this.lista = [];
        this.accion = "LIST";
        this.getQuesosData();
	}
		
	render() {
		return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">        

            <div class="container py-3">
                <div id="quesoList" class="row row-cols-1 row-cols-lg-4 g-4">
                    ${this.lista.sort((a,b) => a.name - b.name).map(function (queso) {
                        return html`<queso-data .queso="${queso}"
                                     @info-queso="${this.informar}"
                                     @elimina-queso="${this.eliminar}"></queso-data>`
                    }.bind(this)
                    )}
                </div>


                <div class="row" >
                    <queso-form id="quesoForm" class="border d-none rounded border-primary"
                        @queso-form-close="${this.quesoFormClose}"
                        @queso-form-store="${this.quesoFormStore}">
                    </queso-form>
                </div>      
                
            </div>
		`;
	}
    
    updated(changedProperties) { 
        if (changedProperties.has("accion")) {
            this.shadowRoot.getElementById("quesoForm").accion = this.accion;
            this.showQuesoBlock(this.accion);
        }
    }
    
    //Carga los datos desde una url que contiene los datos
    getQuesosData() {
        var oReq = new XMLHttpRequest();
        oReq.onload = function() {
            let api = JSON.parse(oReq.responseText);
            this.lista = api.cheeses;
            this.actualizaContador();
        }.bind(this);

        oReq.open("GET", "src/json/quesos.json");
        oReq.send();
    }

    //Cuando se cierra la forma la accion se establece a LIST para que se pueda mostrar la lista
    quesoFormClose() {
        this.accion = "LIST";
    }

    //Se verifica si el nombre ya existe en la lista, si es el caso se edita el contenido en otro caso se agrega el queso a la lista
    quesoFormStore(e) {
        const name = e.detail.queso.name;

        const aux = this.lista.findIndex(queso => queso.name == name);

        console.log(aux);
        if (aux == -1) {
            this.lista = [...this.lista, e.detail.queso];
            this.actualizaContador();
        } else {
            this.lista[aux] = e.detail.queso;
        }
        this.accion = "LIST";
    }    
          
    //Dependiendo de la accion muestra la lista o la forma de edición
    showQuesoBlock() {
        this.shadowRoot.getElementById("queso" + (this.accion == "LIST" ? "List" : "Form")).classList.remove("d-none");
        this.shadowRoot.getElementById("queso" + (this.accion != "LIST" ? "List" : "Form")).classList.add("d-none");	
    }  

    //Asigana el queso a mostrar y coloca la accion como INFO
    informar(e) {
        var queso = e.detail.queso;
        this.shadowRoot.getElementById("quesoForm").queso = queso;
        this.accion = "INFO";
    }

    //Elimina un queso de la lista
    eliminar(e) {
        console.log(e.detail);
        this.lista = this.lista.filter(queso => queso.name != e.detail.name);
        this.actualizaContador();
    }

    actualizaContador() {
        this.dispatchEvent(new CustomEvent("actualiza-contador", {
            bubbles: true, 
            composed: true,
            detail: {
                "contador": this.lista.length
            }
        }));
    }
}
customElements.define('queso-main', QuesoMain)