import { LitElement, html } from 'lit-element';  
import './header-app';
import './queso-main';
import './footer-app';
import './queso-sidebar';

class QuesoApp extends LitElement {
  	
	constructor() {
		// Always calls super() first.
		super();	
	}
		
	render() {
		return html`
			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

			<header-app id="header"></header-app>
			<div class="container-fluid">
				<div class="row">
					<queso-sidebar class="col-1" @new-queso="${this.newQueso}"></queso-sidebar>
					<queso-main class="col-11" @actualiza-contador="${this.actualizaContador}"></queso-main>
				</div>			
			</div>
            <footer-app></footer-app>
		`;
	}

    newQueso(e) {
		console.log("Agregar queso");
        this.shadowRoot.querySelector("queso-main").accion = "NEW";
    }  	

	actualizaContador(e) {
		this.shadowRoot.getElementById("header").contador = e.detail.contador;
	}
}
customElements.define('queso-app', QuesoApp)