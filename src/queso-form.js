import { LitElement, html } from 'lit-element';  

class QuesoForm extends LitElement {
	static get properties() {
		return {	
            nuevo: {type: Boolean},
            queso: {type: Object},
            accion: {type: String}		
		};
	}

	constructor() {
		super();	
        this.nuevo = true;
        this.queso = {};
        this.accion = "NEW";	
	}

    render() {
        return html`	
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
		
            <div class="container">
                <h1>Queso <span class="badge bg-secondary">${this.accion}</span></h1>
                <form>
                    <div class="form-group">
                        <label>Nombre</label>
                        ${
                            this.accion == 'NEW' ? 
                                html`<input type="text" id="quesoFormName" @input="${this.updateName}" class="form-control" placeholder="Nombre" .value="${this.queso.name}">` 
                                :
                                html`<span id="quesoFormName" class="form-control" >${this.queso.name}</span>` 
                        }
                        
                    <div>
                    <div class="form-group">
                        <label>Descripción</label>
                        <textarea class="form-control" @input="${this.updateDescription}" placeholder="Descripción" rows="5" .value="${this.queso.description}"></textarea>
                    <div>
                    <div class="form-group">
                        <label>Edad</label>
                        <input type="number" @input="${this.updateAge}" class="form-control" placeholder="Edad" .value="${this.queso.age}"/>
                        <label>Recomendado</label>
                        <input type="checkbox" @change="${this.updateRecommended}" class="form-control" placeholder="Recomendado" .checked="${this.queso.recommended}"/>
                    <div>
                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                    <button @click="${this.storeQueso}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>			
        `;
    }
    
    updated(changedProperties) { 
        if (changedProperties.has("accion")) {
            if (this.accion == "NEW") {
                this.queso = {};
                this.queso.name = "";
                this.queso.description = "";
                this.queso.age = 0;
                this.queso.recommended = true;
            }
        }
    }

    goBack(e) {
        e.preventDefault();	
        this.dispatchEvent(new CustomEvent("queso-form-close",{}));	
    }    

    updateName(e) {
        console.log(e.target.value);
        this.queso.name = e.target.value;
    }
        
    updateDescription(e) {
        this.queso.description = e.target.value;
    }
        
    updateAge(e) {
        this.queso.age = e.target.value;
    }

    updateRecommended(e) {
        this.queso.recommended = e.target.checked; 
    }
    
    storeQueso(e) {
        e.preventDefault();
        
        this.queso.image = {
            "src": "./img/Caerphilly_cheese.jpg",
            "height": 75,
            "width": 50
        };
                        
        this.dispatchEvent(new CustomEvent("queso-form-store",{
            detail: {
                queso:  {
                        name: this.queso.name,
                        description: this.queso.description,
                        age: this.queso.age,
                        image: this.queso.image,
                        recommended: this.queso.recommended
                    }
                }
            })
        );
    }
        
}  
customElements.define('queso-form', QuesoForm)  