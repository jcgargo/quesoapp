import { LitElement, html, css } from 'lit-element';  

class FooterApp extends LitElement {
    
    static get styles() {
        return css`
        `
    }

  	
	constructor() {
		// Always calls super() first.
		super();	
	}
		
	render() {
		return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">        
        
        <div class="container-fluid bg-primary text-end">
            CDMX
        </div>
		`;
	}
}
customElements.define('footer-app', FooterApp)