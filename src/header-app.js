import { LitElement, html, css } from 'lit-element';  

class HeaderApp extends LitElement {

    static get properties() {
        return {
            contador: {type: Number}
        };
    }
  	
    static get styles() {
        return css`
        `
    }

	constructor() {
		// Always calls super() first.
		super();	
        this.contador = 0;
	}
		
	render() {
		return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">        

			<div class="container-fluid bg-primary">
                <div class="row py-2">
                    <h1 class="col-12">
                        Queso APP <span class="align-middle text-end badge bg-success">Total: ${this.contador}</span>
                    </h1>
                </div>
            </div>
		`;
	}
}
customElements.define('header-app', HeaderApp)